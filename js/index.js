$(function(){
    $('[data-toggle="tooltip"]').tooltip();

    $('.carousel').carousel({
       interval: 4000
    })

    $('#contactanos').on('show.bs.modal', function (e) {
       console.log('El modal se está mostrando');
    
       $('#btcontactanos').removeClass('btn-danger');
       $('#btcontactanos').addClass('btn-outline-warning');
       $('#btcontactanos').prop('disabled',true);
       
    })

    $('#contactanos').on('hidden.bs.modal', function (e) {
       console.log('El modal se cerró');

       $('#btcontactanos').removeClass('btn-outline-warning');
       $('#btcontactanos').addClass('btn-danger');
       $('#btcontactanos').prop('disabled',false);
    
    })
                 
});